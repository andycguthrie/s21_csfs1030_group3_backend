import dotenv from 'dotenv';
import express from "express";
import cors from 'cors';
import authRouter from "./router/auth.js";
import patientsRouter from './router/patients.js';
import adminRouter from './router/admin.js';
import providerRouter from './router/provider.js';
import doctorRouter from './router/doctor.js';
import notesRouter from './router/notes.js';
import historyRouter from './router/history.js';

dotenv.config()

const app = express();
app.use(cors());
const port = process.env.PORT;

app.use(express.json());
app.use(authRouter);
app.use(patientsRouter);
app.use(adminRouter);
app.use(providerRouter)
app.use(notesRouter);
app.use(historyRouter);
app.use(doctorRouter);

app.get("/", (req, res) => res.send("Hello World"));

app.get("*", (req, res) => res.status(404).send("message: not found"));

app.listen(port, () => console.log(`Server is ready on http://localhost:${port}`));