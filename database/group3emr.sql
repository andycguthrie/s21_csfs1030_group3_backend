create database group3emr;
use group3emr;
create table admins (
id int not null primary key auto_increment,
username varchar(255),
userpassword varchar(255)
);
insert into admins (username, userpassword) values ("test", "adminpassword");

create table providers (
id int not null primary key auto_increment,
firstName varchar(255),
lastName varchar(255),
profession varchar(255),
address varchar(255),
phone varchar(255),
username varchar(255),
userpassword varchar(255)
);

insert into providers (
firstName,
lastName,
profession,
address,
phone,
username,
userpassword)
values (
"Doc",
"McDoctorface",
"M.D.",
"123 Main St.",
"555-0001",
"docuser",
"docpassword"
);

create table patient_info (
healthcard varchar(255) not null primary key,
firstName varchar(255),
lastName varchar(255),
DOB date,
address varchar(255),
phone varchar(255)
);

insert into patient_info values (
"123000456000789",
"Jim",
"Smith",
"1970-01-01",
"50 Something Ave.",
"555-9999"
);

create table patient_notes (
id int not null primary key auto_increment,
datecreated date,
content varchar(1000),
created_by_admin int ,
created_by_provider int,
foreign key (created_by_admin) references admins(id),
foreign key (created_by_provider) references providers(id)
);

create table patient_record_history (
id int not null primary key auto_increment,
datecreated date,
content varchar(1000),
created_by_admin int ,
created_by_provider int,
foreign key (created_by_admin) references admins(id),
foreign key (created_by_provider) references providers(id)
);