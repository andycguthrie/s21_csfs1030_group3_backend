import mysql from "mysql";
const connection = mysql.createConnection({
    host: "localhost",
    user: "nodeclient",
    password: "123456",
    database: "group3emr"
})

connection.connect(function (err) {
    if (err) {
        console.error("Error connecting:" + err.stack);
        return;
}
        console.log("Database connected");
})

export default connection;