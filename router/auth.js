import dotenv from "dotenv";
import express from "express";
import jsonwebtoken from "jsonwebtoken";
import argon2 from "argon2";
import { readDoctors } from "../middleware/util/doctorJsonHandler.js";
import { readAdmin } from "../middleware/util/adminJsonHandler.js";

dotenv.config()

const authRouter = express.Router();

authRouter.post("/auth", (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    const auth = async (username, password) => {
        let user = await readDoctors().then(usernameFound => {
            usernameFound = usernameFound.find(obj => obj.username == username)
            return usernameFound
        })

        if (user === undefined) {
            return res.status(401).json({message: 'incorrect credentials provided'})
        }
        const match = await argon2.verify(user.password, password)
        if(match) {
            let token = jsonwebtoken.sign({username}, `${process.env.JWT_SECRET}`);
            return res.send({token}); 
    }
    return res.status(401).send({message: 'incorrect credentials provided'}) 
    } 
    auth (username, password)
})

authRouter.post("/auth_admin", (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    const auth = async (username, password) => {
        let user = await readAdmin().then(usernameFound => {
            usernameFound = usernameFound.find(obj => obj.username == username)
            return usernameFound
        })

        if (user === undefined) {
            return res.status(401).json({message: 'incorrect credentials provided'})
        }
        const match = await argon2.verify(user.password, password)
        if(match) {
            let token = jsonwebtoken.sign({username}, `${process.env.JWT_SECRET}`);
            return res.send({token}); 
    }
    return res.status(401).send({message: 'incorrect credentials provided'}) 
    } 
    auth (username, password)
})

export default authRouter;