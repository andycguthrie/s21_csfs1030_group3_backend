import express from "express";
import { v4 as uuidv4 } from "uuid";
import verifyToken from "../middleware/jwtVerify.js";
import argon2 from "argon2";
import { readAdmin, writeAdmin } from "../middleware/util/adminJsonHandler.js";

const adminRouter = express.Router();

adminRouter.post("/admin", (req, res) => {
    let newAdmin = {
      id: uuidv4(),
      username: req.body.username,
      password: req.body.password,
    }
    argon2.hash(newAdmin.password).then(hash => {
      newAdmin.password = hash
      readAdmin().then((admin) => {
        admin.push(newAdmin);
        writeAdmin(admin);
      const {password, ...omitPassword} = newAdmin;
          return res.status(201).send(omitPassword);
      })
    })
  })

export default adminRouter;