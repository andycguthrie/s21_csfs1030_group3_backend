import express from "express";
import db from '../db.js'

const notesRouter = express.Router();


notesRouter.get('/notes', (req,res) => {
    db.query("SELECT * FROM patient_notes", function (error, results, fields) {
      if (res.status >= 400) {
        alert("Error")
      } else {
        return res.status(201).json(results);
          }
        }
      )
  })

notesRouter.get("/notes/:id", (req, res) => {
    db.query(
      `SELECT * FROM patient_notes WHERE noteid=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });


    notesRouter.post("/notes", (req, res) => {
        db.query(
          "INSERT INTO patient_notes (date, content, created_by) VALUES (?, ?, ?)",
          [req.body.date, req.body.content, req.body.created_by],
          function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
          }
        );
      });
      

export default notesRouter;