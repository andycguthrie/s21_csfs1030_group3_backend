import express from "express";
import { v4 as uuidv4 } from "uuid";
import verifyToken from "../middleware/jwtVerify.js";
import argon2 from "argon2";
import { readDoctors, writeDoctors } from "../middleware/util/doctorJsonHandler.js";
import db from '../db.js'

const doctorRouter = express.Router();

doctorRouter.get('/doctor_results', (req,res) => {
  db.query("SELECT * FROM doctors", function (error, results, fields) {
    if (res.status >= 400) {
      alert("Error")
    } else {
      return res.status(201).json(results);
        }
      }
    )
})

doctorRouter.post("/newDoctor", verifyToken, (req, res) => {
    let newDoctor = {
      id: uuidv4(),
      username: req.body.name,
      password: req.body.password,
    }
    argon2.hash(newDoctor.password).then(hash => {
      newDoctor.password = hash
      readDoctors().then((doctor) => {
        doctor.push(newDoctor);
        writeDoctors(doctor);
      const {password, ...omitPassword} = newDoctor;
          return res.status(201).send(omitPassword);
      })
    })
  })

export default doctorRouter;