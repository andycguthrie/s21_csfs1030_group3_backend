import express from "express";
//import { readProviders, writeProviders } from "../middleware/util/providersJsonHandler.js";
import connection from "../database/connection.js";
//import providers from "../providers.js";

const providerRouter = express.Router();

/*providerRouter.get("/api/provider/:id", (req, res) => {
    let id = req.params.id;
    let result = providers.find((provider) => provider.id == id);
    if (result == undefined) {
        return res.status(404).send("no record")
    } else {
        return res.status(200).send(result);
        }
});
providerRouter.put("/api/provider/:id", async (req, res) => {
    let updatedProvider = JSON.parse(req.body);
    let id = req.params.id;
    let found = providers.indexOf(provider => provider.id == id);
    providers[found] = updatedProvider;
      
    if (found < 0) {
        return res.status(404).json(`'message': 'entry # ${id} not found'`)
    } else {
        return res.status(200).send("record updated");
    }
});

providerRouter.delete("/api/provider/:id", (req, res) => {
    let id = req.params.id;
    let found = providers.indexOf(provider => provider.id == id);
    providers.splice(found, 1);

    if (found < 0) {
        return res.status(404).json(`'message': 'entry # ${id} not found'`)
    } else {
        return res.status(200).send("record updated");
    }
});*/
    providerRouter.get("/api/provider/:id", (req, res) => {
    connection.query(
        `SELECT * FROM providers WHERE id=${req.params.id}`,
        (err, results) => {
            return res.status(200).send(results);
        }
    )
    });

    providerRouter.put("/api/provider/:id", (req, res) => {
        const { firstName, lastName, profession, address, phone } = req.body;
        connection.query(
            `UPDATE providers SET firstName="${firstName}", lastName="${lastName}", profession="${profession}", address="${address}", phone="${phone}" WHERE id=${req.params.id}`,
            function (err, results) {
                if (err) console.error(err);
                return res.status(200).send(results);
            }
        );
    });

    providerRouter.delete("/api/provider/:id", (req, res) => {
        connection.query(
            `DELETE FROM providers WHERE id=${req.params.id}`,
            function (err, results) {
                if (err) console.error(err);
                return res.status(200).send(results);
            }
        );
    });
    
    providerRouter.post("/adminhome/careprovidernew", async (req, res) => {
        let newProvider = JSON.parse(req.body);
       
        return res.status(200);
})



export default providerRouter;