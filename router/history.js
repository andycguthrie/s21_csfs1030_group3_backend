import express from "express";
import db from '../db.js'

const historyRouter = express.Router();


historyRouter.get('/history', (req,res) => {
    db.query("SELECT * FROM patient_history", function (error, results, fields) {
      if (res.status >= 400) {
        alert("Error")
      } else {
        return res.status(201).json(results);
          }
        }
      )
  })

  historyRouter.get("/history/:id", (req, res) => {
    db.query(
      `SELECT * FROM patient_history WHERE historyid=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });

  historyRouter.delete("/history/:id", (req, res) => {
    db.query(
      `DELETE FROM patient_history WHERE historyid=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });


historyRouter.post("/history", (req, res) => {
    db.query(
      "INSERT INTO patient_History (date, action, created_by) VALUES (?, ?, ?)",
      [req.body.date, req.body.action, req.body.created_by],
      function (error, results, fields) {
        if (error) throw error;
        return res.status(201).send(results);
      }
    );
  });
 

      

export default historyRouter;