import dotenv from 'dotenv'
import util from "util";
import fs from "fs";
import path from "path";

dotenv.config()

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const entriesPath = path.resolve(`${process.env.patientsDatabase}`);

async function readEntries() {
  const json = await readFile(entriesPath);
  return JSON.parse(json);
  }
async function writeEntries(entry) {
  const json = JSON.stringify(entry);
  return writeFile(entriesPath, json);
  }
  export { readEntries, writeEntries };