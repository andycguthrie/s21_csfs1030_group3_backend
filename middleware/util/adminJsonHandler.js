import dotenv from 'dotenv'
import util from "util";
import fs from "fs";
import path from "path";

dotenv.config()

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const adminPath = path.resolve(`${process.env.adminsDatabase}`);

async function readAdmin() {
    const json = await readFile(adminPath);
    return JSON.parse(json);
    }
  async function writeAdmin(admin) {
    const json = JSON.stringify(admin);
    return writeFile(adminPath, json);
    }

export { readAdmin, writeAdmin };