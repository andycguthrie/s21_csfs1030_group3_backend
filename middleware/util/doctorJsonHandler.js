import dotenv from 'dotenv'
import util from "util";
import fs from "fs";
import path from "path";

dotenv.config()

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const doctorPath = path.resolve(`${process.env.doctorsDatabase}`);

async function readDoctors() {
    const json = await readFile(doctorPath);
    return JSON.parse(json);
    }
  async function writeDoctors(doctor) {
    const json = JSON.stringify(doctor);
    return writeFile(doctorPath, json);
    }

export { readDoctors, writeDoctors };
