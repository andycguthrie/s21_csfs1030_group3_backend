# s21_csfs1030_group3

Clone Repo

Open terminal or command prompt

run `npm install`

Setup mySQL by following steps in group3emr.sql file

create .env file in backend folder and add the following:

```
DATABASE_USER="nodeclient2021"
DATABASE_PASSWORD="123456"
DATABASE_NAME="group3emr"
```

To start the backend open terminal or command prompt

run `cd backend`

then `npm run dev`

To start the frontend open terminal or command prompt

run `cd frontend`

then `npm start `


**doctor login info:**

username: **test**

password: **password**

**admin login info:**

username: **admin**

password: **password**

